import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    save: function() {
      console.log("Called save action in friends route");
      return true;
    },
    cancel: function() {
      console.log("Called cancel action in friends route");
      return true;
    },
    delete: function(friend) {
      friend.destroyRecord();
      this.transitionTo('friends.index');
    }
  }
});
