import Ember from 'ember';
import formatDate from '../utils/format-date';

function formattedDate(date, format) {
  return formatDate(date, format);
}

export default Ember.Handlebars.makeBoundHelper(formattedDate);
