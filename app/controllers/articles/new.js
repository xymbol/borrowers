import Ember from 'ember';

export default Ember.ObjectController.extend({
  hasDescription: Ember.computed.notEmpty('model.description'),
  isValid: Ember.computed.alias('hasDescription'),
  actions: {
    save: function() {
      if (this.get('isValid')) {
        return true;
      } else {
        this.set('errorMessage', 'Description can\'t be blank');
      }
    }
  }
});
