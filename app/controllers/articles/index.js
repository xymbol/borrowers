import Ember from 'ember';

export default Ember.ArrayController.extend({
  contentDidChange: function() {
    console.log('An article was added or removed');
  }.observes('model.[]'),
  stateDidChange: function() {
    console.log('State changed for an article');
  }.observes('model.@each.state')
});
