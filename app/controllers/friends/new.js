import BaseController from './base';

export default BaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('friends.index');
    }
  }
});
