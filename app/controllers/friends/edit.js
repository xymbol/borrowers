import BaseController from './base';

export default BaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('articles', this.get('model'));
    }
  }
});
